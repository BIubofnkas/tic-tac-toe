package javafxapplication4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class register_user {
    
    
    
    private TTTWebService TTTWebServiceProxy;
    
    
    

    @FXML
    private TextField username_field;

    @FXML
    private PasswordField password_field;
    
    @FXML
    private TextField first_name_field;
    
    @FXML
    private TextField surname_field;
    
    
    @FXML
    private PasswordField password_confirm_field;



    @FXML
    protected void login(ActionEvent event) throws IOException {
        Parent login_parent = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene login_scene = new Scene(login_parent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(login_scene);
        window.setTitle("Tic Tac Toe Login");
        window.show();
    }


    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) throws Exception {


        String username = username_field.getText();
        String first_name = first_name_field.getText();

        String surname = surname_field.getText();

        String password = password_field.getText();
        String password_confirmation = password_confirm_field.getText();


        if(username.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta have a name");

            alert.showAndWait();
        }

        else if(first_name.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a first name");

            alert.showAndWait();
        }
        
        else if(surname.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a surname");

            alert.showAndWait();
        }
         
         
        else if(password.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a password");

            alert.showAndWait();
        }

        else if(password_confirmation.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a password confirmation");

            alert.showAndWait();
        }
        


        else {
            System.out.println("All data entered");

            if (!password.equals(password_confirmation)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Passwords don't match");
                alert.showAndWait();
            }

            else{
                System.out.println("Passwords match");

                
                TTTWebService_Service service = new TTTWebService_Service();
                TTTWebServiceProxy = service.getTTTWebServicePort();
                String register = TTTWebServiceProxy.register(username, password, first_name, surname);
                
                System.out.println("Registration message is: " + register);

                
                if(register == "ERROR-REPEAT"){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("A user already exists with this name");
                    alert.showAndWait();
                }
                else if(register == "ERROR-INSERT"){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("Failed to register user on database");
                    alert.showAndWait();
                }                
                else if(register == "ERROR-RETRIEVE"){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("Failed to retrieve info from database");
                    alert.showAndWait();
                }                              
                else if(register == "ERROR-DB"){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("Could not find database");
                    alert.showAndWait();
                }                 
                else{
                    System.out.println("Registration Successful");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("Registration Successful");
                    alert.showAndWait();
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game_select.fxml"));
                    
                    GameSelect.get_user_id(register);
                    GameSelect.get_username(username);

                    Parent root = fxmlLoader.load();

                    Scene register_scene = new Scene(root);
                    Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                    window.setScene(register_scene);
                    window.setTitle("Login Screen");

                    window.show();
                
                }
                



            }
        }



    }



}