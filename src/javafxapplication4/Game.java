package javafxapplication4;


import javafx.application.Platform;
import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Game {



    private Thread thread;
    private int state_lock_flag = 1;
    private int turn_lock_flag = 0;
    private int game_end_lock = 1;
    private int game_state = 0;


    private TTTWebService TTTWebServiceProxy;
    public static String logged_in_username = "";
    public static String logged_in_user_id = "";
    public static int current_turn = 1;



    public static String game_player1_username = "";
    public static String game_player2_username = "";

    public static String game_player1_id = "";
    public static String game_player2_id = "";

    public static int player1_turns = 0;
    public static int player2_turns = 0;


    public static String gid = "";


    public static void get_logged_in_user_id(String id) {
        logged_in_user_id = id;
    }

    public static void get_gid(String id) {
        gid = id;
    }

    public static void get_logged_in_username(String username){
        logged_in_username = username;
    }



    @FXML
    private TextField info_text;
    @FXML
    private Button top_left_button;
    @FXML
    private Button top_middle_button;
    @FXML
    private Button top_right_button;
    @FXML
    private Button middle_left_button;
    @FXML
    private Button middle_button;
    @FXML
    private Button middle_right_button;
    @FXML
    private Button bottom_left_button;
    @FXML
    private Button bottom_middle_button;
    @FXML
    private Button bottom_right_button;
    @FXML
    private TextField player1_textbox;
    @FXML
    private TextField player1_turns_textbox;
    @FXML
    private TextField player2_textbox;
    @FXML
    private TextField player2_turns_textbox;





    @FXML
    protected void top_left_button_action() {
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 0;
            int y = 2;
            takeSquare(x, y);        
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void top_middle_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 1;
            int y = 2;
            takeSquare(x, y);
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void top_right_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 2;
            int y = 2;
            takeSquare(x, y);
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void middle_left_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 0;
            int y = 1;
            takeSquare(x, y);
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void middle_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 1;
            int y = 1;
            takeSquare(x, y);
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void middle_right_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 2;
            int y = 1;
            takeSquare(x, y);         
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void bottom_left_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 0;
            int y = 0;
            takeSquare(x, y);         
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void bottom_middle_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 1;
            int y = 0;
            takeSquare(x, y);         
            turn_lock_flag = 0;   }

    }

    @FXML
    protected void bottom_right_button_action(){
        if(state_lock_flag == 1 && turn_lock_flag == 1 && game_end_lock == 1){
            int x = 2;
            int y = 0;
            takeSquare(x, y);         
            turn_lock_flag = 0;   }

    }


    @FXML
    protected void return_to_main(ActionEvent event) throws Exception {
        
        //Stop thread before leaving
        game_player1_username = "";
        game_player2_username = "";
        game_player1_id = "";
        game_player2_id = "";
        check_game_state.cancel();
        

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/javafxapplication4/game_select.fxml"));

        Parent root = fxmlLoader.load();

        Scene register_scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Game Select");

        window.show();

    }


    private void takeSquare(int x, int y){

        TTTWebService_Service service = new TTTWebService_Service();
        TTTWebServiceProxy = service.getTTTWebServicePort();
        String take_square = TTTWebServiceProxy.takeSquare(x,y, Integer.parseInt(gid), Integer.parseInt(logged_in_user_id));
        System.out.println("Take square is: " + take_square );
        if ("ERROR-DB".equals(take_square)){
            state_lock_flag = 0;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("ERROR: Cannot access database");
            alert.showAndWait();
        }

        else if ("ERROR-TAKEN".equals(take_square)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("ERROR: Square taken");
            alert.showAndWait();
        }
        else if ("ERROR".equals(take_square)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("ERROR: Just some general error");
            alert.showAndWait();
        }
        else if ("0".equals(take_square)){
            state_lock_flag = 0;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Problem adding to moves table");
            alert.showAndWait();
        }
        else{


            if (x == 0){
                if (y == 0){
                    if(logged_in_user_id.equals(game_player1_id)){
                        bottom_left_button.setText("X");
                    }
                    else{
                        bottom_left_button.setText("O");
                    }
                }
                else if (y == 1){
                    if(logged_in_user_id.equals(game_player1_id)){
                        middle_left_button.setText("X");
                    }
                    else{
                        middle_left_button.setText("O");
                    }
                }
                else{
                    if(logged_in_user_id.equals(game_player1_id)){
                        top_left_button.setText("X");
                    }
                    else{
                        top_left_button.setText("O");
                    }
                }

            }
            else if (x == 1){
                if (y == 0){
                    if(logged_in_user_id.equals(game_player1_id)){
                        bottom_middle_button.setText("X");
                    }
                    else{
                        bottom_middle_button.setText("O");
                    }
                }
                else if (y == 1){
                    if(logged_in_user_id.equals(game_player1_id)){
                        middle_button.setText("X");
                    }
                    else{
                        middle_button.setText("O");
                    }
                }
                else{
                    if(logged_in_user_id.equals(game_player1_id)){
                        top_middle_button.setText("X");
                    }
                    else{
                            top_middle_button.setText("O");
                    }
                }
            }
            else{
                if (y == 0){
                    if(logged_in_user_id.equals(game_player1_id)){
                        bottom_right_button.setText("X");
                    }
                    else{
                        bottom_right_button.setText("O");
                    }
                }
                else if (y == 1){
                    if(logged_in_user_id.equals(game_player1_id)){
                        middle_right_button.setText("X");
                    }
                    else{
                        middle_right_button.setText("O");
                    }
                }
                else{
                    if(logged_in_user_id.equals(game_player1_id)){
                        top_right_button.setText("X");
                    }
                    else{
                        top_right_button.setText("O");
                    }
                }
            }
        }


    }

    @FXML
    public void initialize() {
            System.out.println("Logged in user id in game: " + logged_in_user_id);
            System.out.println("Logged in username in game: " + logged_in_username);

            System.out.println("Game ID: " + gid);
            
            thread = new Thread(check_game_state);            
            //Set thread to close on exit
            thread.setDaemon(true);
            thread.start();


    }

    Task<Void> check_game_state = new Task<Void>() {
        @Override
        protected Void call() throws Exception {
            
            while(true){
                
                //Task is cancelled when user leaves game 
                if (check_game_state.isCancelled()) {
                    game_player1_username = "";
                    game_player2_username = "";
                    game_player1_id = "";
                    game_player2_id = "";
                    System.out.println("Canceling...");
                    //Reset usernames

                    break;
                } 
                //Task not cancelled
                else {
                    TTTWebService_Service service = new TTTWebService_Service();
                    TTTWebServiceProxy = service.getTTTWebServicePort();
                    String game_current_state = TTTWebServiceProxy.getGameState(Integer.parseInt(gid));

                    //Get state from database and check for errors

                    if ("ERROR-DB".equals(game_current_state)){
                        state_lock_flag = 0;
                        Platform.runLater(new Runnable() {
                            @Override public void run() {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setHeaderText(null);
                                alert.setContentText("ERROR: Cannot access database");
                                alert.showAndWait();
                            }
                        });
                    }

                    else if ("ERROR-NOGAME".equals(game_current_state)){
                        state_lock_flag = 0;
                        Platform.runLater(new Runnable() {
                            @Override public void run() {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setHeaderText(null);
                                alert.setContentText("ERROR: Game not found");
                                alert.showAndWait();
                            }
                        });
                    }
                    else if ("-1".equals(game_current_state)){
                        
                        //The game is waiting for player 2
                        state_lock_flag = 0;
                        Platform.runLater(new Runnable() {
                            @Override public void run() {
                                player1_textbox.setText(logged_in_username);
                                info_text.setText("Game can't start until player 2 joins");
                                player2_textbox.setText("Nobody");

                            }
                        });
                    }
                    
                    //THE GAME HAS BEEN WON, LOST, OR TIED
                    else if ("1".equals(game_current_state)){
                        state_lock_flag = 0;
                    }
                    else if ("2".equals(game_current_state)){
                        state_lock_flag = 0;
                    }
                    else if ("3".equals(game_current_state)){
                        state_lock_flag = 0;
                    }
                    
                    
                    
                    else{
                        //No errors, There is a second player in game.

                        state_lock_flag = 1;
                        //Check if both players exists, if not check for players, else check board
                        if("".equals(game_player1_username) || "".equals(game_player2_username) ){
                            System.out.println("Both players aren't saved");
                            //Both players and their ID's have not been saved                     
                            TTTWebServiceProxy = service.getTTTWebServicePort();
                            String full_game_list = TTTWebServiceProxy.showAllMyGames(Integer.parseInt(logged_in_user_id));

                            if("ERROR-NOGAMES".equals(full_game_list)){
                                Platform.runLater(() -> {
                                    info_text.setText("ERROR: Game not found");
                                });
                            }
                            else if("ERROR-DB".equals(full_game_list)){
                                Platform.runLater(() -> {
                                    info_text.setText("ERROR: No database found");
                                });
                            }
                            else{
                                //Split list of games into array of games
                                String[] games_list = full_game_list.split("\\r?\\n");

                                for (String s: games_list) {
                                    //Split single into array game id, username, and time

                                    String[] split_game = s.split(",");
                                    int array_size = split_game.length;

                                    String game_id = split_game[0];
                                    String player1 = split_game[1];
                                    //Originally set player 2 to nobody
                                    String player2 = "Nobody";

                                    //An array size of 4 means a second player has been entered into database
                                    if (array_size == 4){
                                        //Player 2 exists
                                        player2 = split_game[2];

                                    }


                                    if(game_id.equals(gid)){

                                        //This game
                                        String player_2_final = player2;
                                        Platform.runLater(new Runnable() {
                                            @Override public void run() {
                                                player1_textbox.setText(player1);
                                            }
                                        });

                                        Platform.runLater(() -> {
                                            player2_textbox.setText(player_2_final);
                                        });
                                        
                                        
                                        //Found out usernames of both players
                                        game_player1_username = player1;
                                        game_player2_username = player2;


                                        if( game_player1_username.equals(logged_in_username)){
                                            game_player1_id = logged_in_user_id;
                                        }
                                        else{
                                            game_player2_id = logged_in_user_id;
                                        }
                                        //Found out which player the logged in user is and their ID, the other players ID is found after
                                        
                                        System.out.println("Player 1: " + game_player1_username);
                                        System.out.println("Player 2: " + game_player2_username);
                                        System.out.println("Player 1 ID: " + game_player1_id);
                                        System.out.println("Player 2 ID: " + game_player2_id);


                                    }


                                }

                            }                      
                        }
                        else{
                            //Both players and one ID is saved and now the board state can be collected           

                            TTTWebServiceProxy = service.getTTTWebServicePort();
                            String board = TTTWebServiceProxy.getBoard(Integer.parseInt(gid));

                            Thread.sleep(10);

                            if("ERROR-NOMOVES".equals(board)){
                                //No moves yet
                                current_turn = 1;
                                if(game_player1_id == logged_in_user_id && state_lock_flag == 1){
                                    //Check if logged in user is player 1, and therefore can press a button
                                    turn_lock_flag = 1;

                                    Platform.runLater(() -> {
                                        info_text.setText("It is your turn");
                                    });
                                }
                                if(game_player1_id != logged_in_user_id && state_lock_flag == 1){
                                    turn_lock_flag = 0;

                                    Platform.runLater(new Runnable() {
                                      @Override public void run() {
                                        info_text.setText("It is not your turn");
                                      }
                                    });

                                }

                            }
                            else if("ERROR-DB".equals(board)){
                                Platform.runLater(() -> {
                                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.setHeaderText(null);
                                    alert.setContentText("ERROR: Cannot access database");
                                    alert.showAndWait();
                                });
                            }
                            else{
                                TTTWebServiceProxy = service.getTTTWebServicePort();
                                String check_win = TTTWebServiceProxy.checkWin(Integer.parseInt(gid));

                                if("ERROR-RETRIEVE".equals(check_win)){
                                    Platform.runLater(() -> {
                                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                        alert.setHeaderText(null);
                                        alert.setContentText("ERROR: Cannot retrieve win status");
                                        alert.showAndWait();
                                    });
                                }
                                else if("ERROR-NOGAME".equals(check_win)){
                                    Platform.runLater(() -> {
                                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                        alert.setHeaderText(null);
                                        alert.setContentText("ERROR: Cannot retrieve win status. No game found");
                                        alert.showAndWait();
                                    });
                                }
                                else if("ERROR-DB".equals(check_win)){
                                    Platform.runLater(() -> {
                                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                        alert.setHeaderText(null);
                                        alert.setContentText("ERROR: Cannot retrieve win status. No database found");
                                        alert.showAndWait();
                                    });
                                }
                                
                                
                                else if("1".equals(check_win)){                                    
                                    game_end_lock = 0;
                                    game_state = 1;
                                    Platform.runLater(() -> {
                                        info_text.setText("Player 1 has won");
                                    });
                                }
                                else if("2".equals(check_win)){
                                    game_state = 2;
                                    game_end_lock = 0;
                                    Platform.runLater(() -> {
                                        info_text.setText("Player 2 has won");
                                    });
                                }
                                else if("3".equals(check_win)){
                                    game_state = 3;
                                    game_end_lock = 0;
                                    Platform.runLater(() -> {
                                        info_text.setText("The game is a tie");
                                    });
                                }
                                else{
                                    //The game is not over
                                    game_state = 0;
                                    game_end_lock = 1;
                                }
                                
                                //Set the current game state
                                TTTWebServiceProxy = service.getTTTWebServicePort();
                                TTTWebServiceProxy.setGameState(Integer.parseInt(gid), game_state);
                                
                                
                                player1_turns = 0;
                                player2_turns = 0;
                                String[] moves_list = board.split("\\r?\\n");
                                if(moves_list.length > 0){
                                    for (int i =0; i < moves_list.length; i++) {
                                        //Split single into array game id, username, and time

                                        String[] move = moves_list[i].split(",");

                                        String player_id    = move[0];
                                        String x_value      = move[1];
                                        String y_value      = move[2];
                                        //System.out.println("Move by: " +  player_id + " at " + x_value + "," + y_value);

                                        if (player_id.equals(game_player1_id)){
                                            player1_turns++;
                                        }
                                        else if(player_id.equals(game_player2_id)){
                                            player2_turns++;
                                        }
                                        else if("".equals(game_player1_id)){
                                            game_player1_id = player_id;
                                            player1_turns++;
                                        }
                                        else{
                                            game_player2_id = player_id;
                                            player2_turns++;
                                        }
                                        //If it is the most recent turn
                                        if (i == moves_list.length - 1){
                                            

                                            //Here we find the ID's of both players
                                            if (game_player1_id.equals(player_id)){
                                                current_turn = 2;
                                                
                                                //Use last turn to know whos turn it is
                                                if (game_player2_id.equals(logged_in_user_id)){
                                                    turn_lock_flag = 1;
                                                    if(state_lock_flag == 1 && game_end_lock == 1){

                                                        Platform.runLater(() -> {
                                                            info_text.setText("It is your turn");
                                                        });
                                                    }
                                                }
                                                else{
                                                    turn_lock_flag = 0;
                                                    if(state_lock_flag == 1 && game_end_lock == 1){
                                                        Platform.runLater(() -> {
                                                            info_text.setText("It is not your turn");
                                                    });
                                                    }
                                                }
                                            }
                                            else{
                                                current_turn = 1;
                                                 if (game_player1_id.equals(logged_in_user_id)){
                                                    turn_lock_flag = 1;
                                                    if(state_lock_flag == 1 && game_end_lock == 1){
                                                        Platform.runLater(new Runnable() {
                                                          @Override public void run() {
                                                            info_text.setText("It is your turn");
                                                          }
                                                        });
                                                    }
                                                }
                                                else{
                                                    turn_lock_flag = 0;
                                                    if(state_lock_flag == 1 && game_end_lock == 1){
                                                        Platform.runLater(() -> {
                                                            info_text.setText("It is not your turn");
                                                        });
                                                    }
                                                }

                                            }

                                        }
                                        //Counter has been counting turns to display on screen
                                        Platform.runLater(() -> {
                                            player1_turns_textbox.setText("Turn " +String.valueOf(player1_turns));
                                            player2_turns_textbox.setText("Turn " +String.valueOf(player2_turns));
                                        });



                                        //We have an X,Y value to correspond to a location on the board, tis large section places the correct X or O in the correct spaces
                                        if ("0".equals(x_value)){

                                            if ("0".equals(y_value)){

                                                if(player_id.equals(game_player1_id)){
                                                    Platform.runLater(() -> {
                                                        bottom_left_button.setText("X");
                                                    });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        bottom_left_button.setText("O");
                                                    });
                                                }
                                            }
                                            else if ("1".equals(y_value)){
                                                if(player_id.equals(game_player1_id)){
                                                    Platform.runLater(() -> {
                                                        middle_left_button.setText("X");
                                                    });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        middle_left_button.setText("O");
                                                    });
                                                }
                                            }
                                            else{
                                                if(player_id.equals(game_player1_id)){
                                                   Platform.runLater(() -> {
                                                       top_left_button.setText("X");
                                                   });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        top_left_button.setText("O");
                                                    });
                                                }
                                            }

                                        }
                                        else if ("1".equals(x_value)){
                                            if ("0".equals(y_value)){
                                                if(player_id.equals(game_player1_id)){
                                                   Platform.runLater(new Runnable() {
                                                      @Override public void run() {
                                                        bottom_middle_button.setText("X");
                                                      }
                                                    });
                                                }
                                                else{
                                                    Platform.runLater(new Runnable() {
                                                      @Override public void run() {
                                                        bottom_middle_button.setText("O");
                                                      }
                                                    });
                                                }
                                            }
                                            else if ("1".equals(y_value)){
                                                if(player_id.equals(game_player1_id)){
                                                   Platform.runLater(() -> {
                                                       middle_button.setText("X");
                                                   });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        middle_button.setText("O");
                                                    });
                                                }
                                            }
                                            else{
                                                if(player_id.equals(game_player1_id)){
                                                   Platform.runLater(() -> {
                                                       top_middle_button.setText("X");
                                                   });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        top_middle_button.setText("O");
                                                    });
                                                }
                                            }
                                        }
                                        else{
                                            if ("0".equals(y_value)){
                                                if(player_id.equals(game_player1_id)){
                                                   Platform.runLater(() -> {
                                                       bottom_right_button.setText("X");
                                                   });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        bottom_right_button.setText("O");
                                                    });
                                                }
                                            }
                                            else if ("1".equals(y_value)){
                                                if(player_id.equals(game_player1_id)){
                                                   Platform.runLater(() -> {
                                                       middle_right_button.setText("X");
                                                   });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        middle_right_button.setText("O");
                                                    });
                                                }
                                            }
                                            else{
                                                if(player_id.equals(game_player1_id)){
                                                   Platform.runLater(() -> {
                                                       top_right_button.setText("X");
                                                   });
                                                }
                                                else{
                                                    Platform.runLater(() -> {
                                                        top_right_button.setText("O");
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                            }                    
                        }
                    }
                }
            }
        return null;
        }
    };
}
